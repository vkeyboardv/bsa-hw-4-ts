interface IDictionary {
  [key: string]: any;
}

interface ICreateElementProps {
  tagName: string;
  className?: string;
  attributes?: IDictionary;
}

class View {
  element: HTMLElement;

  createElement({ tagName, className = '', attributes = {} }: ICreateElementProps) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;
