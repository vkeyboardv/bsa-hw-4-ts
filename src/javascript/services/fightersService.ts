import { callApi } from '../helpers/apiHelper';

export interface IFighter {
  _id: string;
  name: string;
  source: string;
}

export interface IFighterDetails extends IFighter {
  attack: number|string,
  defense: number|string,
  health: number|string,
  details: {
    attack: number|string,
    defense: number|string,
    health: number|string,
    name?: string,
  }
}

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id: string): Promise<IFighterDetails> {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
