class Fighter {
  public name: string;
  public health: number;
  public attack: number;
  public defense: number;
  constructor(name?: string, health?:number, attack?: number, defense?: number) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
  }

  returnDamage(fighter:any) {
    this.name = fighter.details.name;
    this.health = fighter.details.health;
    this.attack = fighter.details.attack;
    this.defense = fighter.details.defense;

    return this.getHitPower() - this.getBlockPower();
  }

  random = () => Math.floor(Math.random() * 2);

  getHitPower() {
    const criticalHitChance = this.random();

    return this.attack * criticalHitChance;
  }

  getBlockPower() {
    const dodgeChance = this.random();

    return this.defense * dodgeChance;
  }
}

export default Fighter;
